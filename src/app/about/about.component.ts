import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: [
    './about.component.scss',
    './about.component.mobile.scss',
    './about.component.tablet.scss',
  ],
})
export class AboutComponent {}
