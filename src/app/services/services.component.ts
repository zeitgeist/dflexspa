import { Component } from '@angular/core';
import { Service } from 'src/app/services/service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: [
    './services.component.scss',
    './services.component.mobile.scss',
    './services.component.tablet.scss'
  ],
})
export class ServicesComponent {
  public services: Service[] = [
    {
      name: 'Reflexology',
      price: 500,
      duration: '1 hour',
      image: '../assets/services/reflexology.webp',
    } as Service,
    {
      name: 'Swedish w/ Hot Stone Massage',
      price: 500,
      duration: '1 hour',
      image: '../assets/services/swedish.webp',
    } as Service,
    {
      name: 'Combination w/ Hot Stone Massage',
      price: 600,
      duration: '1 hour',
      image: '../assets/services/combination.webp',
    } as Service,
    {
      name: 'Aromatherapy',
      price: 600,
      duration: '1 hour',
      image: '../assets/services/aromatherapy.webp',
    } as Service,
    {
      name: 'Traditional Hilot',
      price: 600,
      duration: '1 hour',
      image: '../assets/services/hilot.webp',
    } as Service,
    {
      name: 'Ventosa',
      price: 600,
      duration: '1 hour',
      image: '../assets/services/ventosa.webp',
    } as Service,
  ];
}
