export interface Service {
  name: string;
  price: number;
  duration: string;
  image: string;
}
