import { Component } from '@angular/core';

// https://dribbble.com/shots/15413140/attachments/7179826?mode=media
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [
    './home.component.scss',
    './home.component.mobile.scss',
    './home.component.tablet.scss'
  ]
})
export class HomeComponent {
}
